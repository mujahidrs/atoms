import 'dart:async';
import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttergeo/camera_screen.dart';
import 'package:fluttergeo/izin_screen.dart';
import 'package:fluttergeo/location_service.dart';
import 'package:fluttergeo/login_screen.dart';
import 'package:fluttergeo/notif.dart';
import 'package:fluttergeo/qrcode_screen.dart';
import 'package:fluttergeo/safe_exam.dart';
import 'package:fluttergeo/ujian_screen.dart';
import 'package:http/http.dart' as http;
import 'package:ntp/ntp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class HomeScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  const HomeScreen({Key? key, required this.cameras}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  LocationService locationService = LocationService();
  double latitude = 0;
  double longitude = 0;

  late TabController _tabController;

  final _tabs = [
    Tab(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [Icon(Icons.check_box), Text("Presensi")],
      ),
    ),
    Tab(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(Icons.indeterminate_check_box_rounded),
          Text("Absensi")
        ],
      ),
    ),
  ];

  late StreamController _presenceController;
  late StreamController _absenceController;

  Future fetchPresence() async {
    try {
      final response = await http.get(Uri.parse(
          '${dotenv.env['IPADDRESS']}/presensi/showByDate.php?status=Hadir&date=${DateTime.now()}'));

      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        // throw Exception('Failed to load presence');
      }
    } catch (e) {
      //tampilkan error di terminal
      // print(e);
      print("fetchPresence: $e");

      showInSnackBar("Failed to load presence");

      // setState(() {
      //   isLoading = false;
      // });

      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text(e.toString()),
      // ));
      // throw Exception('Failed to load post');
    }
  }

  Future fetchAbsence() async {
    try {
      final response = await http.get(Uri.parse(
          '${dotenv.env['IPADDRESS']}/presensi/showByDate.php?status=Absen&date=${DateTime.now()}'));

      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        // throw Exception('Failed to load absence');
      }
    } catch (e) {
      //tampilkan error di terminal
      // print(e);
      print("fetchPresence: $e");

      showInSnackBar("Failed to load absence");

      // setState(() {
      //   isLoading = false;
      // });

      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text(e.toString()),
      // ));
      // throw Exception('Failed to load post');
    }
  }

  loadPresences() async {
    fetchPresence().then((res) async {
      if (!_presenceController.isClosed) {
        _presenceController.add(res);

        var data = res?["data"];

        if (data != null) {
          if (userData != null) {
            var datang = data
                .where((d) =>
                    d["kategori"] == "Datang" && d["id_akun"] == userData["id"])
                .map((d) => d);
            var pulang = data
                .where((d) =>
                    d["kategori"] == "Pulang" && d["id_akun"] == userData["id"])
                .map((d) => d);

            print(datang.toList());
            print(pulang.toList());

            setState(() {
              presensiDatang = datang.toList();
              presensiPulang = pulang.toList();
            });
          }
        }

        return res;
      }
    });
  }

  loadAbsences() async {
    fetchAbsence().then((res) async {
      if (!_absenceController.isClosed) {
        _absenceController.add(res);
        return res;
      }
    });
  }

  Future<void> _handleRefreshPresence() async {
    fetchPresence().then((res) async {
      if (!_presenceController.isClosed) {
        _presenceController.add(res);

        var data = res?["data"];

        if (data != null) {
          if (userData != null) {
            var datang = data
                .where((d) =>
                    d["kategori"] == "Datang" && d["id_akun"] == userData["id"])
                .map((d) => d);
            var pulang = data
                .where((d) =>
                    d["kategori"] == "Pulang" && d["id_akun"] == userData["id"])
                .map((d) => d);

            print(datang.toList());
            print(pulang.toList());

            setState(() {
              presensiDatang = datang.toList();
              presensiPulang = pulang.toList();
            });
          }
        }

        return null;
      }
    });
  }

  Future<void> _handleRefreshAbsence() async {
    fetchAbsence().then((res) async {
      if (!_absenceController.isClosed) {
        _absenceController.add(res);
        return null;
      }
    });
  }

  @override
  void dispose() {
    locationService.dispose();
    _tabController.dispose();
    // _presenceController.close();
    // _absenceController.close();
    super.dispose();
  }

  dynamic userData = {};
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  dynamic presensiDatang = [];
  dynamic presensiPulang = [];
  DateTime waktuserver = DateTime.now();
  bool isLogged = true;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);

    super.initState();

    locationService.locationStream.listen((userLocation) async {
      setState(() {
        latitude = userLocation.latitude;
        longitude = userLocation.longitude;
      });
    });

    if (isLogged) {
      _loadNTPTime();

      readSharedPrefs();
      _presenceController = StreamController();
      _absenceController = StreamController();
      loadPresences();
      loadAbsences();
    }
  }

  Future<void> _loadNTPTime() async {
    [
      'time.google.com',
      'time.facebook.com',
      'time.euro.apple.com',
      'pool.ntp.org',
    ].forEach(_checkTime);
  }

  Future<void> _checkTime(String lookupAddress) async {
    DateTime myTime;
    DateTime ntpTime;

    myTime = DateTime.now();

    try {
      final int offset = await NTP.getNtpOffset(
          localTime: myTime, lookUpAddress: lookupAddress);
      ntpTime = myTime.add(Duration(milliseconds: offset));
      if (mounted) {
        setState(() {
          waktuserver = ntpTime;
        });
      }
    } catch (e) {
      print("loadNTPTime: $e");
      // print(e);

      showInSnackBar("loadNTPTime: $e");
    }
  }

  void readSharedPrefs() async {
    final SharedPreferences prefs = await _prefs;

    if (prefs.getString('userData') != null) {
      if (mounted) {
        setState(() {
          userData = jsonDecode(prefs.getString('userData')!);
        });
      }
    }
  }

  void reportPresent(image) async {
    var now = waktuserver;

    if (userData["pengaturan"]["waktu"] == "local") {
      now = DateTime.now();
    }

    var data = <String, dynamic>{};
    data['latitude'] = latitude.toString();
    data['longitude'] = longitude.toString();
    data['timestamp'] = now.toString();
    data['id_akun'] = userData["id"].toString();
    data['status'] = 'Hadir';
    data['image'] = image.toString();
    data['bukti'] = "";
    data['keterangan'] = "";

    print("${dotenv.env['IPADDRESS']}/presensi/store.php");

    try {
      final response = await http.post(
          Uri.parse("${dotenv.env['IPADDRESS']}/presensi/store.php"),
          body: data);

      print(response.statusCode);

      // cek apakah respon berhasil
      // print(jsonDecode(response.body));
      if (response.statusCode == 200) {
        // final data = jsonDecode(response.body);

        showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: const Text("Berhasil"),
                  content: const Text("Berhasil presensi"),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context, 'Cancel');
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) =>
                                  HomeScreen(cameras: widget.cameras)));
                        },
                        child: const Text("OK"))
                  ],
                ));
        // _pullRefresh();
      } else {
        print(response.body);
      }
    } catch (e) {
      //tampilkan error di terminal
      // print(e);
      print("reportPresent: $e");

      // setState(() {
      //   isLoading = false;
      // });

      showInSnackBar("reportPresent: $e");
    }
  }

  void showInSnackBar(String message) {
    if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(message),
        duration: const Duration(seconds: 1),
      ));
    }
  }

  Future<void> _navigateAndDisplaySelection(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CameraScreen(
                  lensDirection: "front",
                  title: "Presensi Selfie",
                  cameras: widget.cameras,
                  onSubmit: (e, f) {
                    // print(e);
                    // print(f);

                    var now = waktuserver;

                    if (userData["pengaturan"]["waktu"] == "local") {
                      now = DateTime.now();
                    }
                    var batasTerlambat = DateTime(
                        now.year,
                        now.month,
                        now.day,
                        int.parse(userData["pengaturan"]["batas_terlambat"]
                            .split(":")[0]),
                        int.parse(userData["pengaturan"]["batas_terlambat"]
                            .split(":")[1]),
                        int.parse(userData["pengaturan"]["batas_terlambat"]
                            .split(":")[2]),
                        0);

                    if (now.isAfter(batasTerlambat) && presensiDatang.isEmpty) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                title: const Text("Maaf"),
                                content: const Text(
                                    "Anda sudah melebihi batas waktu presensi"),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, 'Cancel');
                                        Navigator.of(context).pushReplacement(
                                            MaterialPageRoute(
                                                builder: (_) => HomeScreen(
                                                    cameras: widget.cameras)));
                                      },
                                      child: const Text("OK"))
                                ],
                              ));
                    } else {
                      reportPresent(e);
                    }
                  },
                  responseMessage:
                      "Berhasil mengambil foto selfie, silahkan SUBMIT",
                )));

    // When a BuildContext is used from a StatefulWidget, the mounted property
    // must be checked after an asynchronous gap.
    if (!mounted) return;

    // After the Selection Screen returns a result, hide any previous snackbars
    // and show the new result.
    // ScaffoldMessenger.of(context)
    //   ..removeCurrentSnackBar()
    //   ..showSnackBar(SnackBar(content: Text('hasil: $result')));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.blue, //or set color with: Color(0xFF0000FF)
    ));

    // print("presensiDatang: " + presensiDatang.toString());

    // return Container();

    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: FloatingActionButton(
          // onPressed: reportPresent,
          onPressed: () {
            var pulangCepat = presensiPulang
                .where((pulang) => pulang["statusPresensi"] == "Pulang Cepat")
                .map((pulang) => pulang);

            var now = waktuserver;

            if (userData["pengaturan"]["waktu"] == "local") {
              now = DateTime.now();
            }

            var batasTerlambat = DateTime(
                now.year,
                now.month,
                now.day,
                int.parse(
                    userData["pengaturan"]["batas_terlambat"].split(":")[0]),
                int.parse(
                    userData["pengaturan"]["batas_terlambat"].split(":")[1]),
                int.parse(
                    userData["pengaturan"]["batas_terlambat"].split(":")[2]),
                0);

            if (now.isAfter(batasTerlambat) && presensiDatang.isEmpty) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                        title: const Text("Maaf"),
                        content: const Text(
                            "Anda sudah melebihi batas waktu presensi"),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context, 'Cancel');
                              },
                              child: const Text("OK"))
                        ],
                      ));
            } else if (pulangCepat.length > 0) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                        title: const Text("Maaf"),
                        content: const Text(
                            "Anda sudah tidak bisa presensi pulang lagi"),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context, 'Cancel');
                              },
                              child: const Text("OK"))
                        ],
                      ));
            } else {
              _navigateAndDisplaySelection(context);
            }
          },
          tooltip: 'Presensi',
          child: const Icon(Icons.fingerprint),
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.blue,
          shape: const CircularNotchedRectangle(),
          notchMargin: 5,
          child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                // IconButton(
                //     tooltip: "Riwayat Presensi",
                //     onPressed: () {
                //       Navigator.of(context).push(MaterialPageRoute(
                //           builder: (_) => NotificationsLog()));
                //     },
                //     icon: const Icon(
                //       Icons.home,
                //       color: Color.fromRGBO(255, 255, 255, 1),
                //     )),
                IconButton(
                    tooltip: "Ujian Online",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => const UjianScreen()));
                    },
                    icon: const Icon(
                      Icons.cast_for_education_rounded,
                      color: Colors.white,
                    )),
                // IconButton(
                //     tooltip: "Presensi Offline",
                //     onPressed: () {
                //       Navigator.of(context).push(MaterialPageRoute(
                //           builder: (_) => const QRCodeScreen()));
                //     },
                //     icon: const Icon(
                //       Icons.qr_code,
                //       color: Colors.white,
                //     )),
                Padding(
                  padding: const EdgeInsets.only(right: 90),
                  child: IconButton(
                    tooltip: "Izin/Sakit",
                    icon: const Icon(
                      Icons.edit_calendar,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => IzinScreen(cameras: widget.cameras)));
                    },
                  ),
                )
              ]),
        ),
        body: Stack(
          children: [
            Container(
              color: Colors.grey.shade200,
            ),
            Positioned(
              top: 27,
              left: 0,
              right: 0,
              child: Container(
                height: 200,
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(35),
                        bottomRight: Radius.circular(35))),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  userData["nama"].toString(),
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                Text(
                                  userData["username"].toString(),
                                  style: const TextStyle(color: Colors.white),
                                ),
                                Text(
                                  userData["nis"].toString(),
                                  style: const TextStyle(color: Colors.white),
                                ),
                                Text(
                                  userData["kelas"].toString(),
                                  style: const TextStyle(color: Colors.white),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "$latitude,$longitude",
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: const [
                                Icon(
                                  Icons.person,
                                  color: Colors.white,
                                ),
                              ],
                            )),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton.icon(
                            icon: const Icon(
                              Icons.location_searching,
                              color: Colors.black,
                              size: 13,
                            ),
                            onPressed: () async {
                              print(
                                  '${dotenv.env['IPADDRESS']}/distance.php?latitudeFrom=${userData["pengaturan"]["latitude"]}&longitudeFrom=${userData["pengaturan"]["longitude"]}&latitudeTo=${latitude.toString()}&longitudeTo=${longitude.toString()}&maximumDistance=${userData["pengaturan"]["jarak_maksimal"]}');

                              try {
                                final response = await http.get(Uri.parse(
                                    '${dotenv.env['IPADDRESS']}/distance.php?latitudeFrom=${userData["pengaturan"]["latitude"]}&longitudeFrom=${userData["pengaturan"]["longitude"]}&latitudeTo=${latitude.toString()}&longitudeTo=${longitude.toString()}&maximumDistance=${userData["pengaturan"]["jarak_maksimal"]}'));

                                if (response.statusCode == 200) {
                                  if (json.decode(response.body)["data"] ==
                                      true) {
                                    if (mounted) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                        backgroundColor: Colors.yellow,
                                        content: Text(
                                          "Lokasi sudah sesuai, siap melakukan presensi",
                                          style: TextStyle(color: Colors.black),
                                        ),
                                        duration: Duration(seconds: 5),
                                      ));
                                    }
                                  } else {
                                    if (mounted) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                        backgroundColor: Colors.red,
                                        content: Text(
                                            "Lokasi belum sesuai, silahkan pindah ke lokasi yang sesuai untuk melakukan presensi"),
                                        duration: Duration(seconds: 5),
                                      ));
                                    }
                                  }

                                  return json.decode(response.body);
                                } else {
                                  // throw Exception('Failed to load presence');
                                }
                              } catch (e) {
                                //tampilkan error di terminal
                                // print(e);
                                print("cekLokasi: $e");

                                showInSnackBar("Failed to load presence");

                                // setState(() {
                                //   isLoading = false;
                                // });

                                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                //   content: Text(e.toString()),
                                // ));
                                // throw Exception('Failed to load presence');
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.yellow),
                            label: const Text(
                              "Cek Lokasi",
                              style:
                                  TextStyle(fontSize: 13, color: Colors.black),
                            )),
                        ElevatedButton.icon(
                            icon: const Icon(
                              Icons.logout,
                              size: 13,
                              color: Colors.black,
                            ),
                            onPressed: () async {
                              final SharedPreferences prefs = await _prefs;

                              prefs.clear();

                              setState(() {
                                isLogged = false;
                              });
                              if (mounted) {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (_) => LoginScreen(
                                            cameras: widget.cameras)));
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.amber),
                            label: const Text(
                              "Keluar",
                              style:
                                  TextStyle(fontSize: 13, color: Colors.black),
                            )),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Positioned(
                top: 150 + 35,
                right: 0,
                left: 0,
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 15),
                                child: Column(
                                  children: [
                                    IntrinsicHeight(
                                      child: Row(
                                        children: [
                                          CheckInfo(
                                              label: "DATANG",
                                              waktuCheck: presensiDatang
                                                      .isNotEmpty
                                                  ? DateFormat.Hm().format(
                                                      DateTime.parse(
                                                          presensiDatang[0]
                                                              ["timestamp"]))
                                                  : "__:__",
                                              waktuIdeal: userData.isNotEmpty
                                                  ? userData["pengaturan"]
                                                          ["tepat_waktu"]
                                                      .substring(
                                                          0,
                                                          userData["pengaturan"]["tepat_waktu"]
                                                                  .length -
                                                              3)
                                                  : "__:__",
                                              status: presensiDatang.isNotEmpty
                                                  ? presensiDatang[0]
                                                      ["statusPresensi"]
                                                  : "Belum"),
                                          const VerticalDivider(),
                                          CheckInfo(
                                              label: "PULANG",
                                              waktuCheck: presensiPulang.isNotEmpty
                                                  ? DateFormat.Hm().format(
                                                      DateTime.parse(
                                                          presensiPulang[presensiPulang.length - 1]
                                                              ["timestamp"]))
                                                  : "__:__",
                                              waktuIdeal: userData.isNotEmpty
                                                  ? userData["pengaturan"]
                                                          ["waktu_pulang"]
                                                      .substring(
                                                          0,
                                                          userData["pengaturan"]["waktu_pulang"]
                                                                  .length -
                                                              3)
                                                  : "__:__",
                                              status: presensiPulang.isNotEmpty
                                                  ? presensiPulang[presensiPulang.length - 1]
                                                      ["statusPresensi"]
                                                  : "Belum")
                                        ],
                                      ),
                                    ),
                                    // const Divider(),
                                    // Row(
                                    //   mainAxisAlignment:
                                    //       MainAxisAlignment.spaceEvenly,
                                    //   children: [
                                    //     IconMenu(
                                    //         onPress: () {},
                                    //         label: "Lembur",
                                    //         icon: Icons.schedule),
                                    //     IconMenu(
                                    //         onPress: () {
                                    //           Navigator.of(context).push(
                                    //               MaterialPageRoute(
                                    //                   builder: (_) =>
                                    //                       IzinScreen(
                                    //                           cameras: widget
                                    //                               .cameras)));
                                    //         },
                                    //         label: "Izin/Sakit",
                                    //         icon: Icons.edit_calendar),
                                    //     IconMenu(
                                    //         onPress: () {},
                                    //         label: "Meeting",
                                    //         icon: Icons.group),
                                    //     IconMenu(
                                    //         onPress: () {},
                                    //         label: "Riwayat",
                                    //         icon: Icons.file_copy),
                                    //   ],
                                    // )
                                  ],
                                )),
                          ),
                          Card(
                            elevation: 2,
                            child: TabBar(
                              onTap: (value) => {
                                // print(value)
                                if (value == 0)
                                  {_handleRefreshPresence()}
                                else
                                  {_handleRefreshAbsence()}
                              },
                              controller: _tabController,
                              indicator: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Colors.blue),
                              labelColor: Colors.white,
                              unselectedLabelColor: Colors.black,
                              tabs: _tabs,
                            ),
                          ),
                        ]))),
            Positioned(
                top: 350,
                left: 20,
                right: 20,
                bottom: 0,
                child: StreamBuilder(
                    stream: _presenceController.stream,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasError) {
                        return Text(snapshot.error.toString());
                      }
                      if (snapshot.hasData) {
                        // print(snapshot.data["data"]);
                        return StreamBuilder(
                            stream: _absenceController.stream,
                            builder: (BuildContext context,
                                AsyncSnapshot snapshot2) {
                              if (snapshot2.hasError) {
                                return Text(snapshot2.error.toString());
                              }
                              if (snapshot2.hasData) {
                                // print(snapshot2.data["data"]);
                                return TabBarView(
                                    controller: _tabController,
                                    children: [
                                      Column(
                                        children: <Widget>[
                                          Row(
                                            children: const [
                                              Text(
                                                "Kehadiran Hari Ini",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: RefreshIndicator(
                                              onRefresh: _handleRefreshPresence,
                                              child: ListView.builder(
                                                physics:
                                                    const AlwaysScrollableScrollPhysics(),
                                                itemCount: snapshot
                                                    .data["data"].length,
                                                itemBuilder: (context, index) {
                                                  index = (snapshot.data["data"]
                                                              .length -
                                                          1) -
                                                      index;

                                                  var presence = snapshot
                                                      .data["data"][index];

                                                  var now = waktuserver;

                                                  if (userData["pengaturan"]
                                                          ["waktu"] ==
                                                      "local") {
                                                    now = DateTime.now();
                                                  }
                                                  var tepatWaktu = DateTime(
                                                      now.year,
                                                      now.month,
                                                      now.day,
                                                      int.parse(userData[
                                                                  "pengaturan"]
                                                              ["tepat_waktu"]
                                                          .split(":")[0]),
                                                      int.parse(userData[
                                                                  "pengaturan"]
                                                              ["tepat_waktu"]
                                                          .split(":")[1]),
                                                      int.parse(
                                                          userData["pengaturan"]
                                                                  ["tepat_waktu"]
                                                              .split(":")[2]),
                                                      0);

                                                  DateTime waktuhadir =
                                                      DateTime.parse(presence[
                                                              "timestamp"])
                                                          .toLocal();

                                                  // var decodedBytes =
                                                  //     base64Decode(
                                                  //         presence["image"]);

                                                  return Card(
                                                    elevation: 3,
                                                    color: presence[
                                                                'on_location'] ==
                                                            true
                                                        ? Colors.green
                                                        : Colors.red,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                    child: Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(15),
                                                        child: Row(
                                                          children: [
                                                            Expanded(
                                                                flex: 5,
                                                                child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        presence['akun']['nama'] ??
                                                                            "No Title",
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: const TextStyle(
                                                                            color:
                                                                                Colors.white,
                                                                            fontSize: 18),
                                                                      ),
                                                                      Text(
                                                                        presence['akun']['kelas'] ??
                                                                            "No Title",
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: const TextStyle(
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      Text(
                                                                        presence['on_location'] ==
                                                                                true
                                                                            ? "Lokasi Sesuai"
                                                                            : "Lokasi Tidak Sesuai",
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: const TextStyle(
                                                                            color:
                                                                                Colors.white),
                                                                      ),
                                                                      Text(
                                                                        waktuhadir
                                                                            .toString(),
                                                                        maxLines:
                                                                            2,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                        style: TextStyle(
                                                                            color: waktuhadir.isAfter(tepatWaktu)
                                                                                ? Colors.yellow
                                                                                : Colors.white),
                                                                      )
                                                                    ])),
                                                            Expanded(
                                                                child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(10),
                                                              child:
                                                                  Image.network(
                                                                "${dotenv.env['IPADDRESS']}/presensi" +
                                                                    "${presence["image"]}",
                                                                // loadingBuilder:
                                                                //     (context,
                                                                //         child,
                                                                //         loadingProgress) {
                                                                //   return CircularProgressIndicator();
                                                                // },
                                                                gaplessPlayback:
                                                                    true,
                                                                errorBuilder:
                                                                    (context,
                                                                        error,
                                                                        stackTrace) {
                                                                  return Icon(
                                                                      Icons
                                                                          .image);
                                                                },
                                                              ),
                                                            ))
                                                          ],
                                                        )),
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Row(
                                            children: const [
                                              Text(
                                                "Ketidakhadiran Hari Ini",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: Scrollbar(
                                              child: RefreshIndicator(
                                                onRefresh:
                                                    _handleRefreshAbsence,
                                                child: ListView.builder(
                                                  physics:
                                                      const AlwaysScrollableScrollPhysics(),
                                                  itemCount: snapshot2
                                                      .data["data"].length,
                                                  itemBuilder:
                                                      (context, index) {
                                                    index = (snapshot2
                                                                .data["data"]
                                                                .length -
                                                            1) -
                                                        index;

                                                    var absence = snapshot2
                                                        .data["data"][index];

                                                    DateTime waktusekarang =
                                                        DateTime.now();
                                                    DateTime waktuideal =
                                                        DateTime.utc(
                                                                waktusekarang
                                                                    .year,
                                                                waktusekarang
                                                                    .month,
                                                                waktusekarang
                                                                    .day,
                                                                -1,
                                                                30)
                                                            .toLocal();
                                                    DateTime waktuhadir =
                                                        DateTime.parse(absence[
                                                                "timestamp"])
                                                            .toLocal();

                                                    // var decodedBytes =
                                                    //     base64Decode(
                                                    //         absence["image"]);

                                                    return Card(
                                                      elevation: 3,
                                                      color: Colors.yellow,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10)),
                                                      child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(15),
                                                          child: Row(
                                                            children: [
                                                              Expanded(
                                                                  flex: 5,
                                                                  child: Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          absence['akun']['nama'] ??
                                                                              "No Title",
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          style: const TextStyle(
                                                                              color: Colors.black,
                                                                              fontSize: 18),
                                                                        ),
                                                                        Text(
                                                                          absence['akun']['kelas'] ??
                                                                              "No Title",
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          style:
                                                                              const TextStyle(color: Colors.black),
                                                                        ),
                                                                        Text(
                                                                          absence[
                                                                              'status'],
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          style:
                                                                              const TextStyle(color: Colors.black),
                                                                        ),
                                                                        Text(
                                                                          waktuhadir
                                                                              .toString(),
                                                                          maxLines:
                                                                              2,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                          style:
                                                                              TextStyle(color: Colors.black),
                                                                        )
                                                                      ])),
                                                              Expanded(
                                                                  child:
                                                                      Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .all(10),
                                                                child: Image
                                                                    .network(
                                                                  "${dotenv.env['IPADDRESS']}/presensi" +
                                                                      "${absence["image"]}",
                                                                  // loadingBuilder:
                                                                  //     (context,
                                                                  //         child,
                                                                  //         loadingProgress) {
                                                                  //   return CircularProgressIndicator();
                                                                  // },
                                                                  gaplessPlayback:
                                                                      true,
                                                                  errorBuilder:
                                                                      (context,
                                                                          error,
                                                                          stackTrace) {
                                                                    return Icon(
                                                                        Icons
                                                                            .image);
                                                                  },
                                                                ),
                                                              ))
                                                            ],
                                                          )),
                                                    );
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ]);
                              }

                              if (snapshot2.connectionState !=
                                  ConnectionState.done) {
                                return RefreshIndicator(
                                    onRefresh: _handleRefreshPresence,
                                    child: const Center(
                                      child: CircularProgressIndicator(),
                                    ));
                              }

                              if (!snapshot2.hasData &&
                                  snapshot2.connectionState ==
                                      ConnectionState.done) {
                                return const Text('No Posts');
                              }
                              return Container();
                            });
                      }
                      if (snapshot.connectionState != ConnectionState.done) {
                        return RefreshIndicator(
                            onRefresh: _handleRefreshPresence,
                            child: const Center(
                              child: CircularProgressIndicator(),
                            ));
                      }

                      if (!snapshot.hasData &&
                          snapshot.connectionState == ConnectionState.done) {
                        return const Text('No Posts');
                      }
                      return Container();
                    }))
          ],
        ));
  }
}

class IconMenu extends StatelessWidget {
  final Function() onPress;

  final IconData? icon;

  final String label;

  const IconMenu(
      {super.key, required this.onPress, this.icon, required this.label});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Column(children: [
        Card(
            elevation: 3,
            color: Colors.green,
            child: Container(
                padding: const EdgeInsets.all(15),
                child: Icon(
                  icon,
                  size: 30,
                  color: Colors.white,
                ))),
        Text(label)
      ]),
    );
  }
}

class CheckInfo extends StatelessWidget {
  final String status;

  final String label;

  final String waktuCheck;

  final String waktuIdeal;

  const CheckInfo(
      {super.key,
      required this.status,
      required this.label,
      required this.waktuCheck,
      required this.waktuIdeal});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(fontSize: 17),
        ),
        const SizedBox(
          height: 5,
        ),
        Wrap(
          children: [
            Text(
              waktuCheck.toString(),
              style: const TextStyle(fontSize: 20),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(
              width: 5,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  waktuIdeal,
                  style: const TextStyle(fontSize: 13),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  status,
                  style: const TextStyle(fontSize: 13),
                  overflow: TextOverflow.ellipsis,
                )
              ],
            )
          ],
        )
      ],
    ));
  }
}
