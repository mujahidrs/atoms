import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttergeo/home_screen.dart';
import 'package:fluttergeo/login_screen.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  const SplashScreen({Key? key, required this.cameras}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  dynamic userData = [];
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  PackageInfo _packageInfo = PackageInfo(
    appName: '',
    packageName: '',
    version: '',
    buildNumber: '',
    buildSignature: '',
    installerStore: '',
  );

  @override
  void initState() {
    super.initState();

    readSharedPrefs();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  void readSharedPrefs() async {
    final SharedPreferences prefs = await _prefs;

    // prefs.clear(); //untuk hapus prefs

    print({prefs});

    if (prefs.getString('userData') != null) {
      setState(() {
        userData = jsonDecode(prefs.getString('userData')!);
      });
    }

    Timer(const Duration(seconds: 5), () {
      if (userData.isNotEmpty) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (_) => HomeScreen(
                  cameras: widget.cameras,
                )));
      } else {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (_) => LoginScreen(
                  cameras: widget.cameras,
                )));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SvgPicture.asset(
              "assets/images/logo_atoms.svg",
              width: 250,
            ),
            const CircularProgressIndicator(),
            Text(
              "v${_packageInfo.version}",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
