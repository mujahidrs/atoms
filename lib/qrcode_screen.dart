import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QRCodeScreen extends StatefulWidget {
  const QRCodeScreen({super.key});

  @override
  State<QRCodeScreen> createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  dynamic userData = {};
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    readSharedPrefs();
  }

  void readSharedPrefs() async {
    final SharedPreferences prefs = await _prefs;

    print({prefs});

    if (prefs.getString('userData') != null) {
      setState(() {
        userData = jsonDecode(prefs.getString('userData')!);
      });
    }

    print(userData[2]);
  }

  // data['kelas'] = userData[3];
  //   data['nis'] = userData[4];
  //   data['nama'] = userData[1];

// "[$userData[1], $userData[2], $userData[3], $userData[4], $userData[5]]"
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Presensi via QRCode")),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            QrImage(
              data:
                  "['${userData[1]}', '${userData[2]}', '${userData[3]}', '${userData[4]}']",
              version: QrVersions.auto,
              // size: 200.0,
            )
          ],
        ));
  }
}
