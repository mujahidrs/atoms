import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttergeo/home_screen.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  const LoginScreen({Key? key, required this.cameras}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isLoading = false;
  bool isSuccess = false;
  bool hidePassword = true;

  PackageInfo _packageInfo = PackageInfo(
    appName: '',
    packageName: '',
    version: '',
    buildNumber: '',
    buildSignature: '',
    installerStore: '',
  );

  void login() async {
    if (nameController.text == "" || passwordController.text == "") {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: const Text("Kesalahan"),
                content: const Text("Username dan password tidak boleh kosong"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context, 'Cancel');
                      },
                      child: const Text("OK"))
                ],
              ));
    } else {
      setState(() {
        isLoading = true;
      });

      final prefs = await SharedPreferences.getInstance();

      try {
        print(
            "${dotenv.env['IPADDRESS']}/akun/login.php?username=${nameController.text}&password=${passwordController.text}");

        dynamic response = await http.get(Uri.parse(
            "${dotenv.env['IPADDRESS']}/akun/login.php?username=${nameController.text}&password=${passwordController.text}"));

        print(response.body);

        if (jsonDecode(response.body)["status"] == 200) {
          final data = jsonDecode(response.body)["data"];

          await prefs.setString("userData", jsonEncode(data));

          if (mounted) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (_) => HomeScreen(
                      cameras: widget.cameras,
                    )));
          }
        }
        if (jsonDecode(response.body)["status"] == 403) {
          print(jsonDecode(response.body)["status"]);
          print(jsonDecode(response.body)["message"]);

          showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                    title: const Text("Kesalahan"),
                    content: Text(jsonDecode(response.body)["message"]),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context, 'Cancel');
                          },
                          child: const Text("OK"))
                    ],
                  ));
        }

        setState(() {
          isLoading = false;
        });
      } catch (e) {
        //tampilkan error di terminal
        print(e);

        setState(() {
          isLoading = false;
        });

        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(e.toString()),
          ));
        }
      }

      // await prefs.setString('username', nameController.text);
      // await prefs.setString('password', passwordController.text);

      // if (nameController.text != "" && passwordController.text != "") {
      //   Navigator.of(context).pushReplacement(
      //       MaterialPageRoute(builder: (_) => const HomeScreen()));
      // } else {
      //   showDialog(
      //       context: context,
      //       builder: (BuildContext context) => AlertDialog(
      //             title: const Text("Kesalahan"),
      //             content: const Text("Username dan password wajib diisi!"),
      //             actions: [
      //               TextButton(
      //                   onPressed: () {
      //                     Navigator.pop(context, 'Cancel');
      //                   },
      //                   child: const Text("OK"))
      //             ],
      //           ));
      // }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(10),
                  child: SvgPicture.asset(
                    "assets/logo_atoms.svg",
                    width: 125,
                  )),
              Column(
                children: [
                  Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(10),
                      child: const Text(
                        'Sign in',
                        style: TextStyle(fontSize: 20),
                      )),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: TextField(
                      autofocus: true,
                      controller: nameController,
                      decoration: InputDecoration(
                        suffix: InkWell(
                          onTap: () {},
                          child: Icon(
                            Icons.visibility,
                            color: Colors.transparent,
                          ),
                        ),
                        labelText: 'Username',
                      ),
                    ),
                  ),

                  Container(
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: TextField(
                      obscureText: hidePassword,
                      controller: passwordController,
                      decoration: InputDecoration(
                        suffix: InkWell(
                          onTap: () {
                            setState(() {
                              hidePassword = !hidePassword;
                            });
                          },
                          child: Icon(
                            hidePassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                            // size: 15,
                          ),
                        ),
                        // border: OutlineInputBorder(),
                        labelText: 'Password',
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  // TextButton(
                  //   onPressed: () {
                  //     //forgot password screen
                  //   },
                  //   child: const Text(
                  //     'Forgot Password',
                  //   ),
                  // ),
                  Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: isLoading
                          ? const CupertinoActivityIndicator(
                              radius: 20,
                              animating: true,
                            )
                          : Row(
                              children: [
                                Expanded(
                                    child: ElevatedButton(
                                        onPressed: login,
                                        child: const Text('Login')))
                              ],
                            )),
                ],
              ),
              // Row(
              //   children: <Widget>[
              //     const Text('Does not have account?'),
              //     TextButton(
              //       child: const Text(
              //         'Sign in',
              //         style: TextStyle(fontSize: 20),
              //       ),
              //       onPressed: () {
              //         //signup screen
              //       },
              //     )
              //   ],
              //   mainAxisAlignment: MainAxisAlignment.center,
              // ),
              SizedBox(
                height: 50,
              ),
              Center(
                child: Text(
                  "v${_packageInfo.version}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
            ],
          )),
    );
  }
}
