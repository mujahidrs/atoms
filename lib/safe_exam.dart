import 'package:flutter/material.dart';

class SafeExam extends StatefulWidget {
  @override
  State<SafeExam> createState() => _SafeExamState();
}

class _SafeExamState extends State<SafeExam> with WidgetsBindingObserver {
  bool _isNotificationOpen = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      // Ketika aplikasi keluar dari layar, lakukan aksi yang diinginkan
      print("Paused");
    } else if (state == AppLifecycleState.inactive) {
      // Ketika aplikasi dikeluarkan dari memori, lakukan aksi yang diinginkan
      print("Inactive");
    } else if (state == AppLifecycleState.resumed) {
      print("Resumed");
    } else if (state == AppLifecycleState.detached) {
      print("Detached");
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // Ketika pengguna mencoba untuk keluar dari aplikasi, lakukan aksi yang diinginkan
        print("Eit");
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
        title: Text("Safe Exam"),
      )),
    );
  }
}
