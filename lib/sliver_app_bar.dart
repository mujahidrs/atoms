import 'dart:math';

import 'package:flutter/material.dart';

class FlappyBird extends StatefulWidget {
  const FlappyBird({super.key});

  @override
  State<FlappyBird> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<FlappyBird> {
  double _birdYPosition = 0;
  double _pipeXPosition = 0;
  double _pipeYPosition = 0;
  double _pipeYPosition2 = 0;
  double _pipeYPosition3 = 0;
  double _pipeYPosition4 = 0;
  double _pipeYPosition5 = 0;
  double _scorePosition = 0;
  bool _isStarted = false;
  final bool _hasCollided = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              'assets/images/Logo_SMK_N_10_Jakarta.jpeg',
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.fill,
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: _buildScore(),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: _buildPipes(),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: _buildBird(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildScore() {
    return Text(
      'score: $_scorePosition',
      style: const TextStyle(
        fontSize: 25,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
    );
  }

  Widget _buildPipes() {
    if (!_hasCollided) {
      _pipeXPosition -= 5;

      if (_pipeXPosition < -50) {
        _pipeXPosition = 400;
        _pipeYPosition = -50 + _randomYPosition();
        _pipeYPosition2 = -50 + _randomYPosition();
        _pipeYPosition3 = -50 + _randomYPosition();
        _pipeYPosition4 = -50 + _randomYPosition();
        _pipeYPosition5 = -50 + _randomYPosition();
        _scorePosition++;
      }
    }

    return Stack(
      children: <Widget>[
        Positioned(
          top: _pipeYPosition,
          left: _pipeXPosition,
          child: Image.asset(
            'assets/flutter_candies_logo.png',
            width: 50,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          top: _pipeYPosition2,
          left: _pipeXPosition + 100,
          child: Image.asset(
            'assets/flutter_candies_logo.png',
            width: 50,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          top: _pipeYPosition3,
          left: _pipeXPosition + 200,
          child: Image.asset(
            'assets/flutter_candies_logo.png',
            width: 50,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          top: _pipeYPosition4,
          left: _pipeXPosition + 300,
          child: Image.asset(
            'assets/flutter_candies_logo.png',
            width: 50,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          top: _pipeYPosition5,
          left: _pipeXPosition + 400,
          child: Image.asset(
            'assets/flutter_candies_logo.png',
            width: 50,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }

  Widget _buildBird() {
    if (!_hasCollided) {
      _birdYPosition += 4;
    }

    return GestureDetector(
      onTap: () {
        _isStarted = true;
        _birdYPosition -= 20;
      },
      child: Image.asset(
        'assets/cypridina.jpeg',
        width: 50,
        height: 50,
        fit: BoxFit.fill,
        color: _isStarted ? Colors.black : Colors.grey,
        colorBlendMode: BlendMode.darken,
        key: UniqueKey(),
        alignment: Alignment(0, _birdYPosition / 100),
      ),
    );
  }

  double _randomYPosition() {
    return 100 + (200 * (Random().nextDouble() - 0.5));
  }
}
