import 'dart:async';
import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttergeo/camera_screen.dart';
import 'package:fluttergeo/components/touchable_field.dart';
import 'package:fluttergeo/home_screen.dart';
import 'package:fluttergeo/location_service.dart';
import 'package:ntp/ntp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class IzinScreen extends StatefulWidget {
  final List<CameraDescription> cameras;
  const IzinScreen({super.key, required this.cameras});

  @override
  State<IzinScreen> createState() => _IzinScreenState();
}

class _IzinScreenState extends State<IzinScreen> {
  String status = "";
  String bukti = "";
  String base64bukti = "";
  String selfie = "";
  String base64selfie = "";
  bool isLogged = true;
  bool isLoading = false;

  LocationService locationService = LocationService();
  double latitude = 0;
  double longitude = 0;

  TextEditingController keteranganController = TextEditingController();

  dynamic userData = {};
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  DateTime waktuserver = DateTime.now();

  @override
  void dispose() {
    locationService.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    locationService.locationStream.listen((userLocation) async {
      setState(() {
        latitude = userLocation.latitude;
        longitude = userLocation.longitude;
      });
    });

    if (isLogged) {
      _loadNTPTime();

      readSharedPrefs();
    }
  }

  void readSharedPrefs() async {
    final SharedPreferences prefs = await _prefs;

    // print({prefs});

    if (prefs.getString('userData') != null) {
      if (mounted) {
        setState(() {
          userData = jsonDecode(prefs.getString('userData')!);
        });
      }
    }

    // print(userData[2]);
  }

  void showInSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(seconds: 1),
    ));
  }

  Future<void> _loadNTPTime() async {
    [
      'time.google.com',
      'time.facebook.com',
      'time.euro.apple.com',
      'pool.ntp.org',
    ].forEach(_checkTime);
  }

  Future<void> _checkTime(String lookupAddress) async {
    DateTime myTime;
    DateTime ntpTime;

    myTime = DateTime.now();

    try {
      final int offset = await NTP.getNtpOffset(
          localTime: myTime, lookUpAddress: lookupAddress);
      ntpTime = myTime.add(Duration(milliseconds: offset));
      if (mounted) {
        setState(() {
          waktuserver = ntpTime;
        });
      }
    } catch (e) {
      print("loadNTPTime: $e");
      // print(e);

      showInSnackBar(e.toString());
    }
  }

  void reportPresent() async {
    setState(() {
      isLoading = true;
    });

    var now = waktuserver;

    if (userData["pengaturan"]["waktu"] == "local") {
      now = DateTime.now();
    }

    var data = <String, dynamic>{};
    data['latitude'] = latitude.toString();
    data['longitude'] = longitude.toString();
    data['timestamp'] = now.toString();
    data['id_akun'] = userData["id"].toString();
    data['status'] = status;
    data['image'] = base64selfie;
    data['bukti'] = base64bukti;
    data['keterangan'] = keteranganController.text.toString();

    print(data);

    print("${dotenv.env['IPADDRESS']}/presensi/store.php");

    try {
      final response = await http.post(
          Uri.parse("${dotenv.env['IPADDRESS']}/presensi/store.php"),
          body: data);

      print(response.body);

      // cek apakah respon berhasil
      // print(jsonDecode(response.body));
      if (jsonDecode(response.body)["status"] == 200) {
        // final data = jsonDecode(response.body);

        showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: const Text("Berhasil"),
                  content: const Text("Berhasil presensi"),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context, 'Cancel');
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) =>
                                  HomeScreen(cameras: widget.cameras)));
                        },
                        child: const Text("OK"))
                  ],
                ));
        // _pullRefresh();
      } else {
        print(response.body);
      }
    } catch (e) {
      //tampilkan error di terminal
      // print(e);
      print("reportPresent: $e");

      setState(() {
        isLoading = false;
      });

      showInSnackBar(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Form Izin/Sakit")),
      body: Container(
          padding: const EdgeInsets.all(20),
          child: Column(children: [
            Expanded(
                child: Column(
              children: [
                TouchableField(
                    placeholder: "Pilih Status",
                    label: "Status",
                    isRequired: true,
                    value: status,
                    onPress: onPressStatus,
                    icon: Icons.chevron_right),
                const SizedBox(
                  height: 10,
                ),
                TouchableField(
                    placeholder: "Bukti Foto",
                    label: "Bukti",
                    isRequired: true,
                    value: bukti,
                    onPress: onPressBukti,
                    icon: Icons.camera_alt),
                const SizedBox(
                  height: 10,
                ),
                TouchableField(
                    placeholder: "Foto Selfie",
                    label: "Selfie",
                    isRequired: true,
                    value: selfie,
                    onPress: onPressSelfie,
                    icon: Icons.camera_front),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: const [
                    Text(
                      "Keterangan",
                      style: TextStyle(fontSize: 15),
                    )
                  ],
                ),
                TextField(
                  controller: keteranganController,
                  maxLines: 2,
                  decoration: const InputDecoration(hintText: "Keterangan"),
                )
              ],
            )),
            Row(
              children: [
                isLoading
                    ? Expanded(
                        child: Center(
                        child: CircularProgressIndicator(),
                      ))
                    : Expanded(
                        child: ElevatedButton(
                        onPressed: () {
                          if (status == "" || bukti == "" || selfie == "") {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                      title: const Text("Maaf"),
                                      content: const Text(
                                          "Silahkan lengkapi formulir yang dibintangi"),
                                      actions: [
                                        TextButton(
                                            onPressed: () {
                                              Navigator.pop(context, 'Cancel');
                                            },
                                            child: const Text("OK"))
                                      ],
                                    ));
                          } else {
                            reportPresent();
                          }
                        },
                        child: const Text("SUBMIT"),
                      ))
              ],
            )
          ])),
    );
  }

  onPressStatus() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (_) {
          return Container(
            height: 200,
            decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(children: [
              Row(
                children: const [
                  Text(
                    "Pilih Status:",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                      child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          onPressed: () {
                            setState(() {
                              status = "Izin";
                            });
                            Navigator.pop(context);
                          },
                          icon: const Icon(
                            Icons.hail_rounded,
                            color: Colors.blue,
                          ),
                          label: const Text(
                            "Izin",
                            style: TextStyle(color: Colors.blue),
                          )))
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: ElevatedButton.icon(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          onPressed: () {
                            setState(() {
                              status = "Sakit";
                            });
                            Navigator.pop(context);
                          },
                          icon: const Icon(
                            Icons.sick,
                            color: Colors.blue,
                          ),
                          label: const Text(
                            "Sakit",
                            style: TextStyle(color: Colors.blue),
                          )))
                ],
              )
            ]),
          );
        });
  }

  onPressBukti() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => CameraScreen(
            lensDirection: "back",
            cameras: widget.cameras,
            title: "Foto Bukti",
            responseMessage: "Berhasil mengambil foto bukti, silahkan SUBMIT",
            onSubmit: (e, f) {
              print(f);
              setState(() {
                base64bukti = e;
                bukti = f;
              });
              Navigator.of(context).pop();
            })));
  }

  onPressSelfie() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => CameraScreen(
            lensDirection: "front",
            cameras: widget.cameras,
            title: "Foto Selfie",
            responseMessage: "Berhasil mengambil foto selfie, silahkan SUBMIT",
            onSubmit: (e, f) {
              print(f);
              setState(() {
                base64selfie = e;
                selfie = f;
              });
              Navigator.of(context).pop();
            })));
  }
}
