import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttergeo/soal_screen.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UjianScreen extends StatefulWidget {
  const UjianScreen({super.key});

  @override
  State<UjianScreen> createState() => _UjianScreenState();
}

class _UjianScreenState extends State<UjianScreen> {
  dynamic userData = {};
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String api =
      "https://script.google.com/macros/s/AKfycbxoJsvu5LIRKvx-a0zpbFEj6y19sXgjQZtFQ0rgszMBsudMajwa_2TkWk3W1w9VIgYxlw/exec";
  List dataujian = [];

  @override
  void initState() {
    readSharedPrefs();
    getUjian();

    Timer.periodic(const Duration(seconds: 1), (timer) {
      readSharedPrefs();
    });

    Timer.periodic(const Duration(seconds: 30), (timer) {
      getUjian();
    });

    super.initState();
  }

  void readSharedPrefs() async {
    final SharedPreferences prefs = await _prefs;

    // print({prefs});

    if (prefs.getString('userData') != null) {
      if (mounted) {
        setState(() {
          userData = jsonDecode(prefs.getString('userData')!);
        });
      }
    }
  }

  Future getUjian() async {
    // setState(() {
    //   isLoading = true;
    // });
    var kelas = userData["kelas"];

    print("${dotenv.env['IPADDRESS']}/ujian/index.php");

    try {
      final response = await http
          .get(Uri.parse("${dotenv.env['IPADDRESS']}/ujian/index.php"));
      final data = jsonDecode(response.body)["data"];

      // cek apakah respon berhasil
      if (jsonDecode(response.body)["status"] == 200) {
        setState(() {
          dataujian = data;
        });
      }
    } catch (e) {
      //tampilkan error di terminal
      print(e);

      // setState(() {
      //   isLoading = false;
      // });

      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text(e.toString()),
      // ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ujian"),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: dataujian.isEmpty
            ? const Text(
                "Belum ada data",
                textAlign: TextAlign.center,
              )
            : ListView.builder(
                itemCount: dataujian.length,
                itemBuilder: (context, index) {
                  if (dataujian[index]["kelas"] == userData["kelas"]) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (_) => SoalScreen(
                                  data: dataujian[index],
                                )));
                      },
                      child: Card(
                        child: ListTile(
                          title: Text(dataujian[index]["mapel"]),
                          subtitle: Text(dataujian[index]["waktu_mulai"] +
                              " - " +
                              dataujian[index]["waktu_selesai"]),
                        ),
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
      ),
    );
  }
}
