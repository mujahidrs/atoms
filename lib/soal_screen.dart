import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:webview_flutter/webview_flutter.dart';

class SoalScreen extends StatefulWidget {
  final dynamic data;

  const SoalScreen({super.key, required this.data});

  @override
  SoalScreenState createState() => SoalScreenState();
}

class SoalScreenState extends State<SoalScreen> with WidgetsBindingObserver {
  Timer? countdownTimer;
  Duration myDuration = const Duration(days: 5);
  AppLifecycleState? _notification;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _notification = state;
    });
  }

  void startTimer() {
    countdownTimer =
        Timer.periodic(const Duration(seconds: 1), (_) => setCountDown());
  }

  // Step 4
  void stopTimer() {
    setState(() => countdownTimer!.cancel());
  }

  // Step 5
  void resetTimer() {
    stopTimer();
    setState(() => myDuration = const Duration(days: 5));
  }

  // Step 6
  void setCountDown() {
    const reduceSecondsBy = 1;
    if (mounted) {
      setState(() {
        final seconds = myDuration.inSeconds - reduceSecondsBy;
        if (seconds < 0) {
          countdownTimer!.cancel();
        } else {
          myDuration = Duration(seconds: seconds);
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // Enable virtual display.
    if (Platform.isAndroid) WebView.platform = AndroidWebView();

    disableCapture();

    WidgetsBinding.instance.addObserver(this);

    startTimer();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Future<bool> _onWillPop(BuildContext context) async {
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text("Back button is disabled")));
    return false;
  }

  @override
  Widget build(BuildContext context) {
    print(_notification);
    DateTime waktuMulai = DateTime.parse(widget.data["waktu_mulai"]).toLocal();
    DateTime waktuSelesai =
        DateTime.parse(widget.data["waktu_selesai"]).toLocal();
    DateTime waktuKini = DateTime.now().toLocal();
    dynamic totalWaktu = waktuSelesai.difference(waktuMulai);

    print("waktu_kini: $waktuKini");
    print("waktu_selesai: $waktuSelesai");
    print("totalWaktu: $totalWaktu");
    // print(totalWaktu)

    String strDigits(int n) => n.toString().padLeft(2, '0');
    // final days = strDigits(myDuration.inDays);
    // Step 7
    final hours = strDigits(myDuration.inHours.remainder(24));
    final minutes = strDigits(myDuration.inMinutes.remainder(60));
    final seconds = strDigits(myDuration.inSeconds.remainder(60));

    return WillPopScope(
        onWillPop: () => _onWillPop(context),
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 100,
            title: Column(
              children: [
            Text(widget.data["mapel"]),
            Text(
              '$hours:$minutes:$seconds',
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 50),
            ),
              ],
            ),
            automaticallyImplyLeading: false,
          ),
          body: WebView(
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: widget.data["link"],
          ),
        ));
  }

  Future<void> disableCapture() async {
    //disable screenshots and record screen in current screen
    await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
  }
}
