import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:fluttergeo/splash_screen.dart';
import 'dart:async';
import 'package:flutter_dotenv/flutter_dotenv.dart';

List<CameraDescription> _cameras = <CameraDescription>[];

void _logError(String code, String? message) {
  if (message != null) {
    print('Error: $code\nError Message: $message');
  } else {
    print('Error: $code');
  }
}

Future<void> main() async {
  // Fetch the available cameras before initializing the app.
  try {
    WidgetsFlutterBinding.ensureInitialized();
    _cameras = await availableCameras();
  } on CameraException catch (e) {
    _logError(e.code, e.description);
  }

  await dotenv.load(fileName: ".env");

  runApp(MyApp(
    cameras: _cameras,
  ));
}

class MyApp extends StatelessWidget {
  final List<CameraDescription> cameras;
  const MyApp({super.key, required this.cameras});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Roboto',
      ),
      home: SplashScreen(
        cameras: cameras,
      ),
    );
  }
}
