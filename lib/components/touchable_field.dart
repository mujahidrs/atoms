import 'package:flutter/material.dart';

class TouchableField extends StatelessWidget {
  final Function() onPress;

  final IconData icon;

  final String label;

  final String value;

  final String placeholder;

  final bool isRequired;

  const TouchableField(
      {super.key,
      required this.onPress,
      required this.icon,
      required this.label,
      required this.value,
      required this.placeholder,
      required this.isRequired});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Column(children: [
        Row(
          children: [
            Text(
              label,
              style: const TextStyle(fontSize: 15),
            ),
            const Text(
              " *",
              style: TextStyle(color: Colors.red),
            )
          ],
        ),
        Container(
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(width: 1, color: Colors.grey))),
          // color: Colors.green,
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Wrap(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        value == "" ? placeholder : value,
                        style: TextStyle(
                            color: value == ""
                                ? const Color(0x99000000)
                                : Colors.black,
                            fontSize: 16),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                    Icon(icon)
                  ],
                )
              ]),
        )
      ]),
    );
  }
}
